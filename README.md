# Sqrrl Task App

App in which dog's breed and their images are listed.

 - Splash
 - Home Screen (List of dog's breed)
 - Image Screen (List of selected breed images)


 ## Dependencies:

  - androidx.lifecycle:lifecycle-extensions :
    Combine library for `Viewmodel` and `LiveData` package.
  - com.github.bumptech.glide:glide :
    image loading library.
  - com.squareup.retrofit2:converter-gson :
    Combine library for `Retrofit` & `GSON` package.
  - io.reactivex.rxjava2:rxjava :
    RxJava Package (used for observing api response)
  - io.reactivex.rxjava2:rxandroid :
    RxJava helper pacakge for android.
  - com.jakewharton.retrofit:retrofit2-rxjava2-adapter :
    Used for `RxJava2CallAdapterFactory` class to allow service to return `Single<>` `Observable` etc
  - com.google.dagger:dagger :
    Dagger used for dependency inject.
  - javax.annotation:jsr250-api :
    annotation library package.
    Internally used by dagger in generated class for annotations. (used `@Generated` annotation).

 
 ![splash](https://i.ibb.co/6ZL4sbx/0015c9c5a013a8111553750529.png)

![home](https://i.ibb.co/BTTrcL7/8945c9c5a2824cd41553750568.png)

![imagelist](https://i.ibb.co/YQP0zt0/6935c9c5a41a27bf1553750593.png)

