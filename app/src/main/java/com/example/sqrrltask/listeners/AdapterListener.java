package com.example.sqrrltask.listeners;

public interface AdapterListener<T> {

    void onClick(T t);
}
