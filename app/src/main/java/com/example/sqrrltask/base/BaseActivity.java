package com.example.sqrrltask.base;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity extends AppCompatActivity {
    ViewDataBinding viewDataBinding;
    private boolean isBackEnabled;

    public abstract int getLayoutResource();

    public abstract void onViewReady();

    public abstract String getToolBarTitle();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutResource());
        if (getToolBarTitle() != null) {
            setToolbarTitle(getToolBarTitle());
        }
        onViewReady();
    }

    protected void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    protected void setBackEnable() {
        isBackEnabled = true;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public ViewDataBinding getViewDataBinding() {
        return viewDataBinding;
    }

    public BaseApplication getApp() {
        return (BaseApplication) getApplication();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (isBackEnabled) {
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
