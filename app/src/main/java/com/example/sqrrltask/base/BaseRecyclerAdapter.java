package com.example.sqrrltask.base;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.sqrrltask.listeners.AdapterListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseRecyclerAdapter<Data, VH extends BaseRecyclerViewHolder<Data>> extends RecyclerView.Adapter<VH> {

    protected AdapterListener onItemClickListener;
    ArrayList<Data> datas;

    public BaseRecyclerAdapter(ArrayList<Data> datas) {
        this.datas = datas;
    }

    public BaseRecyclerAdapter(ArrayList<Data> datas, AdapterListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.datas = datas;
    }

    public void setDatas(ArrayList<Data> datas) {
        this.datas = datas;
    }

    public ArrayList<Data> getDatas() {
        return datas;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), getItemResourceLayout(), viewGroup, false);
        return onCreateViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull VH viewHolder, int position) {
        viewHolder.bindData(datas.get(position));
    }

    @Override
    public int getItemCount() {
        try {
            return datas.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public abstract VH onCreateViewHolder(ViewDataBinding viewDataBinding);

    public abstract int getItemResourceLayout();


}
