package com.example.sqrrltask.base;

import android.view.View;

import com.example.sqrrltask.listeners.AdapterListener;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseRecyclerViewHolder<Data> extends RecyclerView.ViewHolder implements View.OnClickListener {

    private AdapterListener listener;
    private ViewDataBinding viewDataBinding;

    public BaseRecyclerViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.viewDataBinding = binding;

    }

    public BaseRecyclerViewHolder(ViewDataBinding binding, AdapterListener listener) {
        super(binding.getRoot());
        this.viewDataBinding = binding;
        binding.getRoot().setOnClickListener(this);
        this.listener = listener;
    }

    public ViewDataBinding getViewDataBinding() {
        return viewDataBinding;
    }

    public abstract void bindData(Data item);

    @Override
    public void onClick(View view) {
        if (this.listener != null) {
            this.listener.onClick(getLayoutPosition());
        }
    }
}
