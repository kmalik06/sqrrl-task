package com.example.sqrrltask.base;

import android.app.Application;

import com.example.sqrrltask.di.component.ApiComponent;
import com.example.sqrrltask.di.component.DaggerApiComponent;
import com.example.sqrrltask.di.module.ApiModule;

public class BaseApplication extends Application {

    private ApiComponent apiComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        setApiComponent(DaggerApiComponent
                .builder()
                .apiModule(new ApiModule(this))
                .build());
    }


    public void setApiComponent(ApiComponent apiComponent) {
        this.apiComponent= apiComponent;
    }

    public ApiComponent getApiComponent() {
        return apiComponent;
    }
}
