package com.example.sqrrltask.base;

import android.app.Application;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

public abstract class AppViewModel extends AndroidViewModel {
    protected MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    protected MutableLiveData<String> message = new MutableLiveData<>();
    protected CompositeDisposable disposable = new CompositeDisposable();
    @Inject
    public Retrofit retrofit;

    public AppViewModel(@NonNull Application application) {
        super(application);
        ((BaseApplication) application).getApiComponent().inject(this);
    }


    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public LiveData<String> getMessage() {
        return message;
    }
}
