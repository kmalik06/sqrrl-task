package com.example.sqrrltask.views;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sqrrltask.R;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.sqrrltask.base.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    public int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    public void onViewReady() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        }, 3000);
    }

    @Override
    public String getToolBarTitle() {
        return null;
    }

}
