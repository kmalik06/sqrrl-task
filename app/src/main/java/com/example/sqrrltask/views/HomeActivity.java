package com.example.sqrrltask.views;

import android.content.Intent;
import android.view.View;

import com.example.sqrrltask.R;
import com.example.sqrrltask.adapters.BreedListAdapter;
import com.example.sqrrltask.base.BaseActivity;
import com.example.sqrrltask.databinding.ActivityHomeBinding;
import com.example.sqrrltask.listeners.AdapterListener;
import com.example.sqrrltask.viewmodels.HomeViewModel;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class HomeActivity extends BaseActivity {
    ActivityHomeBinding activityHomeBinding;
    HomeViewModel homeViewModel;
    View.OnClickListener onClickListener = v -> {
        switch (v.getId()) {
            case R.id.tv_error:
                homeViewModel.fetchBreedList();
                break;
        }
    };
    SwipeRefreshLayout.OnRefreshListener refreshListener = () -> {
        if (homeViewModel != null)
            homeViewModel.fetchBreedList();
    };
    AdapterListener<Integer> adapterListener = pos -> {
        if (homeViewModel.getBreedList() != null && homeViewModel.getBreedList().getValue() != null && homeViewModel.getBreedList().getValue().size() > pos)
            startActivity(new Intent(this, BreedImagesActivity.class).putExtra("breedName", homeViewModel.getBreedList().getValue().get(pos)));
    };

    @Override
    public int getLayoutResource() {
        return R.layout.activity_home;
    }

    @Override
    public void onViewReady() {
        activityHomeBinding = (ActivityHomeBinding) getViewDataBinding();
        activityHomeBinding.setLifecycleOwner(this);
        homeViewModel = ViewModelProviders.of(this, new ViewModelProvider.AndroidViewModelFactory(getApp())).get(HomeViewModel.class);
        activityHomeBinding.setIsLoading(homeViewModel.getIsLoading());
        activityHomeBinding.setError(homeViewModel.getMessage());
        activityHomeBinding.setClickListener(onClickListener);
        activityHomeBinding.setRefreshListener(refreshListener);
        setObserver();
    }

    @Override
    public String getToolBarTitle() {
        return getString(R.string.dog_s_breed_list);
    }

    private void setObserver() {
        homeViewModel.getBreedList().observe(this, breedList -> {
            if (breedList != null) {
                activityHomeBinding.setAdapter(new BreedListAdapter(breedList, adapterListener));
            }
        });
    }

}
