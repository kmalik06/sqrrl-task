package com.example.sqrrltask.views;

import android.view.View;

import com.example.sqrrltask.R;
import com.example.sqrrltask.adapters.ImageListAdapter;
import com.example.sqrrltask.base.BaseActivity;
import com.example.sqrrltask.databinding.ActivityBreedImagesBinding;
import com.example.sqrrltask.viewmodels.BreedImagesViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class BreedImagesActivity extends BaseActivity {
    private ActivityBreedImagesBinding breedImagesBinding;
    private BreedImagesViewModel breedImagesViewModel;
    private String breedName;
    private ImageListAdapter imageListAdapter;
    private View.OnClickListener onClickListener = v -> {
        switch (v.getId()) {
            case R.id.tv_error:
                if (breedImagesViewModel != null) {
                    breedImagesViewModel.fetchImageList(breedName);
                }
                break;
        }
    };

    private SwipeRefreshLayout.OnRefreshListener refreshListener = () -> {
        if (breedImagesViewModel != null) {
            breedImagesViewModel.fetchImageList(breedName);
        }

    };

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            int lastVisibleItemPosition = 0;
            if (layoutManager != null)
                lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

            int size = imageListAdapter.getDatas().size();
            if (lastVisibleItemPosition == imageListAdapter.getDatas().size() - 1 && size < breedImagesViewModel.getPageSize()) {
                ArrayList<String> imageList = imageListAdapter.getDatas();
                List<String> newData = breedImagesViewModel.getNextData(size);
                imageList.addAll(size, newData);
                imageListAdapter.notifyItemRangeInserted(size, newData.size());
            }
        }
    };

    @Override
    public int getLayoutResource() {
        return R.layout.activity_breed_images;
    }

    @Override
    public void onViewReady() {
        breedImagesBinding = (ActivityBreedImagesBinding) getViewDataBinding();
        breedImagesViewModel = ViewModelProviders.of(this, new ViewModelProvider.AndroidViewModelFactory(getApp())).get(BreedImagesViewModel.class);
        setBackEnable();
        breedImagesBinding.setLifecycleOwner(this);
        breedImagesBinding.setIsLoading(breedImagesViewModel.getIsLoading());
        breedImagesBinding.setError(breedImagesViewModel.getMessage());
        breedImagesBinding.setClickListener(onClickListener);
        breedImagesBinding.setRefreshListener(refreshListener);
        breedImagesBinding.setOnScrollListener(scrollListener);
        if (getIntent().getExtras() != null) {
            breedName = getIntent().getExtras().getString("breedName");
            setToolbarTitle(breedName);
        }
        setObserver();

    }

    @Override
    public String getToolBarTitle() {
        return null;
    }


    private void setObserver() {
        breedImagesViewModel.getImageList(breedName).observe(this, imageList -> {
            if (imageList != null) {

                imageListAdapter = new ImageListAdapter(imageList);
                breedImagesBinding.setAdapter(imageListAdapter);
            }
        });
    }

}
