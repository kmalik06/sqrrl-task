package com.example.sqrrltask.bindings;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.sqrrltask.R;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class Bindings {

    @BindingAdapter({"swipeEnable"})
    public static void setSwipeEnable(SwipeRefreshLayout refreshLayout, Boolean isEnable) {
        refreshLayout.setEnabled(isEnable);
    }

    @BindingAdapter({"isRefresh"})
    public static void setIsRefresh(SwipeRefreshLayout refreshLayout, LiveData<Boolean> isEnable) {
        if (isEnable.getValue() != null)
            refreshLayout.setRefreshing(isEnable.getValue());
    }

    @BindingAdapter({"onRefreshListener"})
    public static void setOnRefreshListener(SwipeRefreshLayout refreshLayout, SwipeRefreshLayout.OnRefreshListener refreshListener) {
        if (refreshLayout != null)
            refreshLayout.setOnRefreshListener(refreshListener);
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView imageView, String imageUrl) {
        Glide.with(imageView.getContext().getApplicationContext()).load(imageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_picture).diskCacheStrategy(DiskCacheStrategy.ALL)).into(imageView);
    }

    @BindingAdapter({"scrollListener"})
    public static void setScrollListener(RecyclerView recyclerView, RecyclerView.OnScrollListener scrollListener) {
        recyclerView.addOnScrollListener(scrollListener);
    }
}
