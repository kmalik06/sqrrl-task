package com.example.sqrrltask.viewmodels;

import android.app.Application;

import com.example.sqrrltask.api.ApiService;
import com.example.sqrrltask.base.AppViewModel;
import com.example.sqrrltask.models.BaseResponse;
import com.example.sqrrltask.models.BreedListResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends AppViewModel {
    private MutableLiveData<ArrayList<String>> breedList;

    public HomeViewModel(Application application) {
        super(application);
    }

    public void fetchBreedList() {
        isLoading.setValue(true);
        ApiService apiService = retrofit.create(ApiService.class);
        disposable.add(apiService.getBreedList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<BreedListResponse>() {
                    @Override
                    public void onSuccess(BreedListResponse s) {
                        breedList.setValue(s.getBreedList());
                        message.setValue("");
                        isLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        message.setValue(e.getMessage());
                        isLoading.setValue(false);
                    }
                }));
    }


    public LiveData<ArrayList<String>> getBreedList() {
        if (breedList == null) {
            breedList = new MutableLiveData<>();
            fetchBreedList();
        }
        return breedList;
    }


}
