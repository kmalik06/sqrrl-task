package com.example.sqrrltask.viewmodels;

import android.app.Application;
import android.util.Log;

import com.example.sqrrltask.api.ApiService;
import com.example.sqrrltask.base.AppViewModel;
import com.example.sqrrltask.models.ImageListResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BreedImagesViewModel extends AppViewModel {
    private MutableLiveData<ArrayList<String>> imageList;
    ArrayList<String> storedImages;
    private int pageSize;
    private final int pageLimit = 10;

    public BreedImagesViewModel(@NonNull Application application) {
        super(application);
    }

    public void fetchImageList(String breedName) {
        if (breedName == null)
            return;
        isLoading.setValue(true);
        ApiService apiService = retrofit.create(ApiService.class);
        disposable.add(apiService.getImageList(breedName).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<ImageListResponse>() {
                    @Override
                    public void onSuccess(ImageListResponse s) {
                        if (s.getImageList() != null) {
                            storedImages = s.getImageList();
                            ArrayList<String> images = new ArrayList<>();
                            images.addAll(getNextData(0));
                            imageList.setValue(images);
                            pageSize = storedImages.size();
                        }
                        message.setValue("");
                        isLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        message.setValue(e.getMessage());
                        isLoading.setValue(false);
                    }
                }));
    }


    public LiveData<ArrayList<String>> getImageList(String breedName) {
        if (imageList == null) {
            imageList = new MutableLiveData<>();
            fetchImageList(breedName);
        }
        return imageList;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List<String> getNextData(int size) {
        int pageSizeDiff = storedImages.size() - size;
        int lastIndex = pageSizeDiff > pageLimit ? pageLimit : pageSizeDiff;
        Log.d("lastindex", "getNextData: "+lastIndex);
        return storedImages.subList(size, size + lastIndex);
    }
}
