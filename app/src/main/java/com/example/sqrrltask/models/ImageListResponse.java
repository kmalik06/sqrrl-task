package com.example.sqrrltask.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ImageListResponse extends BaseResponse {
    @SerializedName("message")
    ArrayList<String> imageList;


    public ArrayList<String> getImageList() {
        return imageList;
    }
}
