package com.example.sqrrltask.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BreedListResponse extends BaseResponse {
    @SerializedName("message")
    JsonObject breedList;


    public ArrayList<String> getBreedList() {
        return new ArrayList<String>(breedList.keySet());

    }
}
