package com.example.sqrrltask.adapters;

import com.example.sqrrltask.R;
import com.example.sqrrltask.base.BaseRecyclerAdapter;
import com.example.sqrrltask.base.BaseRecyclerViewHolder;
import com.example.sqrrltask.databinding.BreedItemLayoutBinding;
import com.example.sqrrltask.listeners.AdapterListener;

import java.util.ArrayList;

import androidx.databinding.ViewDataBinding;

public class BreedListAdapter extends BaseRecyclerAdapter<String, BreedListAdapter.ViewHolder> {


    private final AdapterListener<Integer> adapterListener;

    public BreedListAdapter(ArrayList<String> lists, AdapterListener<Integer> adapterListener) {
        super(lists, adapterListener);
        this.adapterListener = adapterListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewDataBinding viewDataBinding) {
        return new ViewHolder(viewDataBinding, adapterListener);
    }

    @Override
    public int getItemResourceLayout() {
        return R.layout.breed_item_layout;
    }


    public class ViewHolder extends BaseRecyclerViewHolder<String> {

        public ViewHolder(ViewDataBinding binding, AdapterListener<Integer> listener) {
            super(binding, listener);
        }

        @Override
        public void bindData(String item) {
            ((BreedItemLayoutBinding) getViewDataBinding()).setBreedName(item);
        }
    }
}
