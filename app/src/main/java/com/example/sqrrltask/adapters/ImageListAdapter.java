package com.example.sqrrltask.adapters;

import com.example.sqrrltask.R;
import com.example.sqrrltask.base.BaseRecyclerAdapter;
import com.example.sqrrltask.base.BaseRecyclerViewHolder;
import com.example.sqrrltask.databinding.ImageListItemBinding;

import java.util.ArrayList;

import androidx.databinding.ViewDataBinding;

public class ImageListAdapter extends BaseRecyclerAdapter<String, ImageListAdapter.ViewHolder> {

    public ImageListAdapter(ArrayList<String> datas) {
        super(datas);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewDataBinding viewDataBinding) {
        return new ViewHolder(viewDataBinding);
    }

    @Override
    public int getItemResourceLayout() {
        return R.layout.image_list_item;
    }

    public class ViewHolder extends BaseRecyclerViewHolder<String> {

        public ViewHolder(ViewDataBinding binding) {
            super(binding);
        }

        @Override
        public void bindData(String item) {
            ((ImageListItemBinding) getViewDataBinding()).setImageUrl(item);
        }
    }
}
