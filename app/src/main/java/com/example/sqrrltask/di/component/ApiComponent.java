package com.example.sqrrltask.di.component;


import com.example.sqrrltask.base.AppViewModel;
import com.example.sqrrltask.di.ApiScope;
import com.example.sqrrltask.di.module.ApiModule;
import com.example.sqrrltask.viewmodels.HomeViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by intel on 07-Jul-17.
 */

@ApiScope
@Singleton
@Component(modules = {ApiModule.class})
public interface ApiComponent {
    void inject(HomeViewModel homeViewModel);

    void inject(AppViewModel appViewModel);
}
