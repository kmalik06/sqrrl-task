package com.example.sqrrltask.di.module;

import android.app.Application;


import com.example.sqrrltask.api.ApiService;
import com.example.sqrrltask.api.interceptor.ApiInterceptor;
import com.example.sqrrltask.api.interceptor.ConnectivityInterceptor;
import com.example.sqrrltask.base.BaseApplication;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import androidx.annotation.Nullable;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by intel on 08-Jul-17.
 */
@Module
public class ApiModule {
    public static final long DEFAULT_OK_HTTP_CACHE_SIZE = 10 * 1024 * 1024;
    public static final long DEFAULT_OK_HTTP_TIMEOUT_SECONDS = 150;
    public static final long DEFAULT_OK_HTTP_REQUEST_TIMEOUT_SECONDS = DEFAULT_OK_HTTP_TIMEOUT_SECONDS;
    public static final long DEFAULT_OK_HTTP_RESPONSE_TIMEOUT_SECONDS = DEFAULT_OK_HTTP_TIMEOUT_SECONDS;
    private final BaseApplication application;

    public ApiModule(BaseApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Application application) {
        Cache cache = new Cache(application.getCacheDir(), DEFAULT_OK_HTTP_CACHE_SIZE);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .cache(cache)
                .readTimeout(DEFAULT_OK_HTTP_REQUEST_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_OK_HTTP_RESPONSE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(DEFAULT_OK_HTTP_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(new ConnectivityInterceptor(application.getApplicationContext()))
                .addInterceptor(new ApiInterceptor());

        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .baseUrl(ApiService.BASE_URL).build();
    }

}
