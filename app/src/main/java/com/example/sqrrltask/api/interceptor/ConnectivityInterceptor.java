package com.example.sqrrltask.api.interceptor;

import android.content.Context;


import com.example.sqrrltask.R;
import com.example.sqrrltask.api.exceptions.NoConnectivityException;
import com.example.sqrrltask.utils.Utility;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;


public class ConnectivityInterceptor implements Interceptor {
    private Context mContext;

    public ConnectivityInterceptor(Context context) {
        mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!Utility.getConnectivityStatus(mContext)) {
            throw new NoConnectivityException(mContext.getString(R.string.no_network));
        }
        return chain.proceed(chain.request());
    }
}
