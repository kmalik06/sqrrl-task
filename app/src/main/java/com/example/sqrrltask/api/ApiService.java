package com.example.sqrrltask.api;

import com.example.sqrrltask.models.BreedListResponse;
import com.example.sqrrltask.models.ImageListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    String BASE_URL = "https://dog.ceo/api/";

    @GET("breeds/list/all")
    Single<BreedListResponse> getBreedList();
    @GET("breed/{breedName}/images")
    Single<ImageListResponse> getImageList(@Path("breedName") String breedName);
}
