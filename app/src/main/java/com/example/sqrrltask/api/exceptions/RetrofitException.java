package com.example.sqrrltask.api.exceptions;

import java.io.IOException;


public class RetrofitException extends IOException
{
    private String message;

    public RetrofitException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
