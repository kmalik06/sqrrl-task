package com.example.sqrrltask.api.exceptions;

import java.io.IOException;


public class NoConnectivityException extends IOException
{
    String message;

    public NoConnectivityException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
