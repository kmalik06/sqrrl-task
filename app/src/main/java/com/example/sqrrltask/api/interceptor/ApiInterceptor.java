package com.example.sqrrltask.api.interceptor;

import com.example.sqrrltask.api.exceptions.RetrofitException;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        switch (response.code()) {
            case 200:

                break;
            default:
                throw new RetrofitException("Something went wrong");
        }
        return response;
    }
}
